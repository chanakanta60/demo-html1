var data_import = [{
        "month": "January",
        "year": 2015,
        "export": 91847,
        "import": 41206,
        "tradebalance": 50641
    },
    {
        "month": "February",
        "year": 2015,
        "export": 89265,
        "import": 35911,
        "tradebalance": 53354
    },
    {
        "month": "March",
        "year": 2015,
        "export": 105526,
        "import": 38315,
        "tradebalance": 67211
    },
    {
        "month": "April",
        "year": 2015,
        "export": 94948,
        "import": 41964,
        "tradebalance": 52984
    },
    {
        "month": "May",
        "year": 2015,
        "export": 106776,
        "import": 37341,
        "tradebalance": 69435
    },
    {
        "month": "June",
        "year": 2015,
        "export": 100471,
        "import": 37098,
        "tradebalance": 63373
    },
    {
        "month": "July",
        "year": 2015,
        "export": 103255,
        "import": 37765,
        "tradebalance": 65490
    },
    {
        "month": "August",
        "year": 2015,
        "export": 101373,
        "import": 38923,
        "tradebalance": 62450
    },
    {
        "month": "September",
        "year": 2015,
        "export": 102246,
        "import": 37942,
        "tradebalance": 64304
    },
    {
        "month": "October",
        "year": 2015,
        "export": 110696,
        "import": 39309,
        "tradebalance": 71387
    },
    {
        "month": "November",
        "year": 2015,
        "export": 98434,
        "import": 36954,
        "tradebalance": 61480
    },
    {
        "month": "December",
        "year": 2015,
        "export": 108092,
        "import": 42986,
        "tradebalance": 65106
    },
    {
        "month": "January",
        "year": 2016,
        "export": 94939,
        "import": 44226,
        "tradebalance": 50713
    },
    {
        "month": "February",
        "year": 2016,
        "export": 98230,
        "import": 36171,
        "tradebalance": 62059
    },
    {
        "month": "March",
        "year": 2016,
        "export": 113115,
        "import": 43798,
        "tradebalance": 69317
    },
    {
        "month": "April",
        "year": 2016,
        "export": 95754,
        "import": 41561,
        "tradebalance": 54193
    },
    {
        "month": "May",
        "year": 2016,
        "export": 104244,
        "import": 40153,
        "tradebalance": 64091
    },
    {
        "month": "June",
        "year": 2016,
        "export": 100579,
        "import": 40553,
        "tradebalance": 60026
    },
    {
        "month": "July",
        "year": 2016,
        "export": 89340,
        "import": 37621,
        "tradebalance": 51719
    },
    {
        "month": "August",
        "year": 2016,
        "export": 99964,
        "import": 41540,
        "tradebalance": 58424
    },
    {
        "month": "September",
        "year": 2016,
        "export": 100228,
        "import": 42824,
        "tradebalance": 57404
    },
    {
        "month": "October",
        "year": 2016,
        "export": 99474,
        "import": 42279,
        "tradebalance": 57195
    },
    {
        "month": "November",
        "year": 2016,
        "export": 109125,
        "import": 41972,
        "tradebalance": 67153
    },
    {
        "month": "December",
        "year": 2016,
        "export": 113014,
        "import": 45019,
        "tradebalance": 67995
    },
    {
        "month": "January",
        "year": 2017,
        "export": 101704,
        "import": 42291,
        "tradebalance": 59413
    },
    {
        "month": "February",
        "year": 2017,
        "export": 110668,
        "import": 39926,
        "tradebalance": 70742
    },
    {
        "month": "March",
        "year": 2017,
        "export": 127149,
        "import": 47041,
        "tradebalance": 80108
    },
    {
        "month": "April",
        "year": 2017,
        "export": 107441,
        "import": 40240,
        "tradebalance": 67201
    },
    {
        "month": "May",
        "year": 2017,
        "export": 122187,
        "import": 47581,
        "tradebalance": 74606
    },
    {
        "month": "June",
        "year": 2017,
        "export": 113967,
        "import": 43959,
        "tradebalance": 70008
    },
    {
        "month": "July",
        "year": 2017,
        "export": 111223,
        "import": 40612,
        "tradebalance": 70611
    },
    {
        "month": "August",
        "year": 2017,
        "export": 119655,
        "import": 41634,
        "tradebalance": 78021
    },
    {
        "month": "September",
        "year": 2017,
        "export": 108031,
        "import": 38523,
        "tradebalance": 69508
    },
    {
        "month": "October",
        "year": 2017,
        "export": 105331,
        "import": 41092,
        "tradebalance": 64239
    },
    {
        "month": "November",
        "year": 2017,
        "export": 124026,
        "import": 44871,
        "tradebalance": 79155
    },
    {
        "month": "December",
        "year": 2017,
        "export": 112869,
        "import": 39902,
        "tradebalance": 72967
    },
    {
        "month": "January",
        "year": 2018,
        "export": 108601,
        "import": 44072,
        "tradebalance": 64529
    },
    {
        "month": "February",
        "year": 2018,
        "export": 102450,
        "import": 39642,
        "tradebalance": 62808
    },
    {
        "month": "March",
        "year": 2018,
        "export": 108701,
        "import": 44333,
        "tradebalance": 64368
    },
    {
        "month": "April",
        "year": 2018,
        "export": 101148,
        "import": 42354,
        "tradebalance": 58794
    },
    {
        "month": "May",
        "year": 2018,
        "export": 109728,
        "import": 46176,
        "tradebalance": 63552
    },
    {
        "month": "June",
        "year": 2018,
        "export": 109028,
        "import": 39215,
        "tradebalance": 69813
    },
    {
        "month": "July",
        "year": 2018,
        "export": 105886,
        "import": 36066,
        "tradebalance": 69820
    },
    {
        "month": "August",
        "year": 2018,
        "export": 117532,
        "import": 47005,
        "tradebalance": 70527
    },
    {
        "month": "September",
        "year": 2018,
        "export": 105272,
        "import": 42481,
        "tradebalance": 62791
    },
    {
        "month": "October",
        "year": 2018,
        "export": 115007,
        "import": 43276,
        "tradebalance": 71731
    },
    {
        "month": "November",
        "year": 2018,
        "export": 110090,
        "import": 45013,
        "tradebalance": 65077
    },
    {
        "month": "December",
        "year": 2018,
        "export": 103718,
        "import": 41474,
        "tradebalance": 62244
    },
    {
        "month": "January",
        "year": 2019,
        "export": 101601,
        "import": 54569,
        "tradebalance": 47032
    },
    {
        "month": "February",
        "year": 2019,
        "export": 95836,
        "import": 37306,
        "tradebalance": 58530
    },
    {
        "month": "March",
        "year": 2019,
        "export": 104850,
        "import": 38866,
        "tradebalance": 65984
    },
    {
        "month": "April",
        "year": 2019,
        "export": 94267,
        "import": 45112,
        "tradebalance": 49155
    },
    {
        "month": "May",
        "year": 2019,
        "export": 105513,
        "import": 46306,
        "tradebalance": 59207
    },
    {
        "month": "June",
        "year": 2019,
        "export": 96005,
        "import": 38993,
        "tradebalance": 57012
    },
    {
        "month": "July",
        "year": 2019,
        "export": 95850,
        "import": 40528,
        "tradebalance": 55322
    },
    {
        "month": "August",
        "year": 2019,
        "export": 95075,
        "import": 39932,
        "tradebalance": 55143
    },
    {
        "month": "September",
        "year": 2019,
        "export": 92956,
        "import": 37936,
        "tradebalance": 55020
    },
    {
        "month": "October",
        "year": 2019,
        "export": 103428,
        "import": 40684,
        "tradebalance": 62744
    },
    {
        "month": "November",
        "year": 2019,
        "export": 98932,
        "import": 40432,
        "tradebalance": 58500
    },
    {
        "month": "December",
        "year": 2019,
        "export": 92928,
        "import": 43738,
        "tradebalance": 49190
    }
]